#!/usr/bin/env python

import sys
import ROOT

ROOT.gROOT.SetBatch(True)

def main():
  # get the fit result
  result_HL = GetResult('FitCrossChecks_Run2Sys_HL.root')
  result_IC = GetResult('FitCrossChecks_ICHEP.root')
  #print 'The HL-LHC correlation: %f'%result_HL.correlation("SigXsecOverSMWH","SigXsecOverSMZH")
  #print 'The ICHEP  correlation: %f'%result_IC.correlation("SigXsecOverSMWH","SigXsecOverSMZH")
  result_IC.Print()

  # get the correlation
  corr_HL = ROOT.TLatex(1.15,1.36,'#color[2]{corr in HighL: %.3f}'%result_HL.correlation("SigXsecOverSMWH","SigXsecOverSMZH"))
  corr_IC = ROOT.TLatex(1.15,1.44, '#color[1]{corr in ICHEP: %.3f}'%result_IC.correlation("SigXsecOverSMWH","SigXsecOverSMZH"))
  corr_HL.SetTextSize(0.03)
  corr_IC.SetTextSize(0.03)

  # create frame
  frame = ROOT.RooPlot(0.5,1.5,0.5,1.5) 
  frame.SetTitle("Covariance ZH(Y) vs. WH(X)")

  # plot on frame
  plot_HL = result_HL.plotOn(frame,"SigXsecOverSMWH","SigXsecOverSMZH",'MEAB')
  #plot_HL.Print()
  plot_HL.getAttLine("contour").SetLineColor(ROOT.kRed)
  plot_HL.getAttMarker("TMarker").SetMarkerColor(ROOT.kRed)

  plot_IC = result_IC.plotOn(frame,"SigXsecOverSMWH","SigXsecOverSMZH")

  c = ROOT.TCanvas("2d contour","2d",800,800)

  frame.Draw()

  # Draw lines
  Linex = ROOT.TLine(1.0,0.5,1.0,1.5)
  Liney = ROOT.TLine(0.5,1.0,1.5,1.0)
  Linex.SetLineStyle(ROOT.kDashed)
  Liney.SetLineStyle(ROOT.kDashed)
  Linex.Draw()
  Liney.Draw()

  # Draw the text
  corr_HL.Draw()
  corr_IC.Draw()

  # Get frame
  file_demo = ROOT.TFile.Open("myfile.root")
  frame_demo = file_demo.Get('frame_2de99c0')
  #frame_demo.Draw()
  graph = frame_demo.getAttLine("contour_nll_model_modelData_n1.000000")
  graph.SetLineColor(ROOT.kOrange)
  graph.Draw('lsame')

  c.Print('corrWHZH.pdf')

def GetResult(name):
  f = ROOT.TFile.Open(name)
  result = f.Get('PlotsAfterFitToAsimov/unconditionnal/fitResult')
  return result

def GetFrame(name):
  f = ROOT.TFile.Open(name)
  frame = f.Get('frame_2de99c0')
  return frame

if __name__=="__main__":
    main()
